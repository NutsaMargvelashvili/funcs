function calcAverage(scores){
    let sum = 0;
    for(let i = 0; i < scores.length; i++){
       sum += scores[i];
    }
    return sum / scores.length;
}
function checkWinner(avg1, avg2){
    if(avg1 >= (avg2 * 2)){
        console.log("Power Rangers are winner winner chicken dinner");
    }
    else if(avg2 >= (avg1 * 2)){
        console.log("Fairy Tails are winner winner chicken dinner");
    }
    else if(avg1 === avg2){
        console.log("Draw");
    }
    else{
        console.log("We have no winner :/");
    }
    return 0;
}
let competition = [];
competition[0] = {
    PowerRangers: [44, 23, 71],
    FairyTails: [65, 54, 49]
};
competition[1] = {
    PowerRangers: [85, 54, 41],
    FairyTails: [23, 34, 47]
};

checkWinner(calcAverage(competition[0].PowerRangers), calcAverage(competition[0].FairyTails));
checkWinner(calcAverage(competition[1].PowerRangers), calcAverage(competition[1].FairyTails));

/************************************/

function calcTip(bill){ 
    let tip = ((bill > 50) && (bill < 300)) ? (bill * 15) / 100 : (bill * 20) / 100;
    return tip;
}
function Avg(x){
    let sum = 0;
    for(let i = 0; i < x.length; i++){
       sum += x[i];
    }
    return sum / x.length;
}
let Ludovico = {
    Bill: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52] 
};

Ludovico.Tip = [];
for(let num of Ludovico.Bill){
    Ludovico.Tip.push(calcTip(num));
}
console.log(Ludovico.Tip);

Ludovico.BillPlusTip = [];
for(let i = 0; i < Ludovico.Bill.length; i++){
    Ludovico.BillPlusTip.push((Ludovico.Bill[i] + Ludovico.Tip[i]));
}
console.log(Ludovico.BillPlusTip);

console.log(`Ludovico has left a ${Avg(Ludovico.Tip)}$ tip.`);
console.log(`Ludovico has spent ${Avg(Ludovico.BillPlusTip)}$ on the whole.`);

// console.log(Ludovico);
